<?php
/**
 * Enqueue script and styles for child theme
 */
function woodmart_child_enqueue_styles() {
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'woodmart-style' ), woodmart_get_theme_info( 'Version' ) );
}
add_action( 'wp_enqueue_scripts', 'woodmart_child_enqueue_styles', 10010 );

add_filter( 'woocommerce_get_price_html', 'QuadLayers_remove_price');
function QuadLayers_remove_price($price){     
     return ;
}

add_filter( 'woocommerce_cart_needs_payment', '__return_false' );