<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', '' );

/** Database username */
define( 'DB_USER', '' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '?UtqP=F~x8|2 gh_m,TP5IGeIN%:Q 2]w+7x|p5,sksX;vy(nhHIL>R<?:q|9i(O' );
define( 'SECURE_AUTH_KEY',  'O`4%1c?gR[.p*7&9Fc=_!o-?-*d5.ts$YQff(mT@#wC,Yz0.OJElO+YM<ZYt7yB|' );
define( 'LOGGED_IN_KEY',    'jpBfX*7yZNl>;c`;Or,xOUhLU<tNZJ@se[eMVRCMuqwK;Cy~aZ,_53~Qx19L3v3&' );
define( 'NONCE_KEY',        '=-aj0X z9D;;to{y2jF@(M/K68^D&almqm&OxjD4()`1&6~_y-Lsl*zy)h^J8f)o' );
define( 'AUTH_SALT',        '%O/S=+g@fy7%D%]jGf@sotus#r=-IjxL<=gYImIYX3 ~[tgJmVthN5_eXk5xDh@I' );
define( 'SECURE_AUTH_SALT', 'eXHRg+BNReEHT5Ep^V+8?f5OBj-S5s&QLk# S!g^EpbKqxe15X#A)Z<z~{FVGPWc' );
define( 'LOGGED_IN_SALT',   'K&[`8EL}3Z7Bsx`B?Q:[)_`o9j>X&0E}BT]1u|C6<gr0[FcfP;N;I/w`&qOzv?IH' );
define( 'NONCE_SALT',       'VU>DY/9@dpxM>X?aLL`UVKzLnQ:T>W_0l|{y^,O|nT`R&37O;^1k]5WyEa2MQ/[5' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
